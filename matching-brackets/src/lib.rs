/// Check brackets are correctly matched and balanced
pub fn brackets_are_balanced(string: &str) -> bool {
    println!("1 - Unedited string: {}", string);
    let chars: Vec<&str> = string.split("").filter(|x| !x.trim().is_empty()).collect();
    println!("2 - Split string into Vec of chars: {:?}", chars);
    let non_raw = filter_non_bracket_chars(chars);
    println!("3 - Filtered char-vec: {:?}", non_raw);

    match non_raw.len() {
        x if x == 0 => return true,
        x if x % 2 != 0 => return false,
        _ => return check_pairs(non_raw),
    }
}

fn check_pairs(brackets: Vec<&str>) -> bool {
    let _reduced_pairs = reduce_pairs(brackets);
    true
}

fn reduce_pairs(brackets: Vec<&str>) -> Vec<&str> {
    brackets
}

fn filter_non_bracket_chars(raw_chars: Vec<&str>) -> Vec<&str> {
    let matches = vec!["[", "]", "{", "}", "(", ")"];
    return raw_chars
        .into_iter()
        .filter(|ch| matches.into_iter().any(|m| *ch == m))
        .collect();
}
